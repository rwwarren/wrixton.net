<!doctype html>
<html>
<head>
<title>Ryan Warren Resume</title>
<style media="screen" type="text/css">
html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed,
figure, figcaption, footer, header, hgroup,
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
  margin: 0;
  padding: 0;
  border: 0;
  font-size: 100%;
  vertical-align: baseline;
}
/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure,
footer, header, hgroup, menu, nav, section {
  display: block;
}
body {
  line-height: 1;
}
blockquote, q {
  quotes: none;
}
blockquote:before, blockquote:after,
q:before, q:after {
  content: '';
  content: none;
}
table {
  border-collapse: collapse;
  border-spacing: 0;
}

body {
  font: 12px Verdana;
}

p {
  margin-top: 0;
  margin-bottom: .35em;
}

h1 {
  font: 1.4em Verdana;
  margin-bottom: .5em;
}

h2 {
  font: Bold 1.2em Verdana;
  text-transform: uppercase;
  border-bottom:1px #dddddd solid;
  margin-top: 1.0em;
  margin-bottom: .35em;
}

h3 {
  font: Bold 1.0em Verdana;
  text-transform: uppercase;
}

#content {
  width: 800px;
  margin: 0 auto;
}

#header {
  text-align: center;
}

#content > #experience > ul {
  list-style: disc inside;
}

#content > div > ul {
  list-style: none;
}

#content > #experience > ul > li {
  margin-bottom: 0.2em;
}

#content > div > ul > li {
  margin-bottom: 0.8em;
}

ul ul {
  list-style: disc inside;
}

.for {
  font: bold 1.0em verdana;
  text-transform: uppercase;
}

.for span:before {
  margin: 0 10px;
  content: '-';
}

.for span {
  font-weight: normal;
  text-transform: none;
}

.as {
  font: normal 1.0em verdana;
  text-transform: uppercase;
}

ul {
  margin-left: .5em;
  padding-left: .5em;
}

li {
  margin-bottom: .2em;
}
</style>
</head>
<body>
  <div id="content">
    <div id="header">
      <h1>Ryan Warren</h1>
      <p>Computer Science Engineering - University of Washington</p>
      <p>ryan.ww@gmail.com</p>
    </div>
    <div id="experience">
      <h2>Technical Experience</h2>
      <ul>
        <li>Web programming experience wih Linux, Apache, MySQL, and PHP</li>
        <li>Fascinated by server administration</li>
        <li>Desire to develop more web &amp; mobile programming experience</li>
        <li>Comfortable with bash and source control using git</li>
        <li>Favorite text-editor is Vim</li>
      </ul>
    </div>
    <div id="work">
      <h2>Relevant Work Experience</h2>
      <ul>
        <li>
          <div class="for">University of Washington<span>June 2012 to Present</span></div>
          <div class="as">Technical Student Assistant</div>
          <ul>
            <li>Imaging and configuring machines for students' use</li>
            <li>Troubleshooting application and network issues</li>
            <li>Installing and administering physical servers</li>
          </ul>
        </li>
      </ul>
    </div>
    <div id="activities">
      <h2>Related Activities</h2>
      <ul>
        <li>
          <h3>Web Programming:</h3>
          <ul>
            <li>Running <a href="http://wrixton.net">wrixton.net</a>, a personal website, for seven years</li>
            <li>Developing simple user-authenticated websites using PHP and MySQL</li>
            <li>Setting up and hosting forums using phpBB</li>
            <li>Creating personal blog and fraternity website using WordPress</li>
            <li>Experience using git with bitbucket for source control</li>
          </ul>
        </li>
      </ul>
      <ul>
        <li>
          <h3>Server Administration:</h3>
          <ul>
            <li>Configured a physical server from scratch with Ubuntu and Apache</li>
            <li>Set up an Amazon ec2 linux server with Apache</li>
          </ul>
        </li>
      </ul>
      <ul>
        <li>
          <h3>School Projects:</h3>
          <ul>
            <li>Implemented a HashMap with an array using separate chaining</li>
            <li>Created a Heap with a comparator and deque to solve a maze using an array </li>
            <li>Drew stars on a drawingpanel using guava collections</li>
            <li>Used the Gale-Sharpley algorithm to create a marriage solver</li>
            <li>Implemented breadth first serach, depth first search, and Dijkstra's algorithm to a graph class</li>
            <li>Modified a quicksort algorithm, also used runtimes to show what sorting algorithm was picked</li>
          </ul>
        </li>
      </ul>
    </div>
    <div id="education">
      <h2>Education</h2>
      <ul>
        <li>
          <h3>University of Washington:</h3>
          <ul>
            <li>Candidate for Bachelor of Computer Science Engineering, anticipating June 2014 graduation</li>
            <li>Learned about data structures, algorithms, and object oriented programming in Computer Programming I &amp; II</li>
          </ul>
        </li>
        <li>
          <h3>Udacity:</h3>
          <ul>
            <li>Introduction to Computer Science: Used Python to build a simple web crawler</li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</body>
</html>
