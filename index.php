<?php
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
<!--<LINK href="css/styles.css" rel="stylesheet" type="text/css">-->
<link rel="stylesheet" type="text/css" media="screen" href="/css/coda-slider.css">
<meta charset="utf-8">

<title>Wrixton.net</title>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
<script src="/js/jquery-1.8.3.js"></script>
<script src="./js/jquery-ui-1.8.20.custom.min.js"></script>
<script src="/js/jquery.coda-slider-3.0.js"></script>
<script>
  $(function() {
    //$('#slider-id').codaSlider();
    $('#slider-id').codaSlider({
      //autoSlide:true,
      //autoHeight:false
    });
  });
</script>

</head>
<body>
<p>


<body>
<div id="wrapper">
<div class="coda-slider" id="slider-id">
  <div>
    <h2 class="title">Content</h2>
    <p>Here is my site!</p>
    <p>My name is Ryan Warren</p>
    <p>I go to University of Washington</p>
    <p>This is a simple test to learn jQuery and mess around with it</p>
    <p>This also has a list to my linkedin, github, bitbucket, stuff I built,
        and my resume</p>
  </div>
  <div>
    <h2 class="title">Linkedin</h2>
    <p>Feel free to look at my Linkedin profile.</p>
    <p><a href="http://www.linkedin.com/pub/ryan-warren/35/954/738">My Linkedin</a></p>
  </div>
  <div>
    <h2 class="title">Github</h2>
    <p>This is a link to my github!</p>
    <p><a href="https://github.com/rwwarren">Github Repo</a></p>
  </div>
  <div>
    <h2 class="title">Bitbucket</h2>
    <p>Take a look at my bitbucket repo!</p>
    <p><a href="https://bitbucket.org/rwwarren">Bitbucket Repo</a></p>
  </div>
  <div>
    <h2 class="title">Stuff I've built</h2>
    <p>This is coming soon!</p>
  </div>
  <div>
    <h2 class="title">Resume</h2>
    <p>Here is a brief summary of my resume</p>
    <p>Web programming experience wih Linux, Apache, MySQL, and PHP</p>
    <p>Running a website</p>
    <p>Configured a physical server from scratch with Ubuntu and Apache</p>
<p><a href="/resume/">Link to my Resume</a></p>
  </div>
</div>



</div>
</body>
</html>
